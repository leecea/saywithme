//
//  SettingsVC.h
//  SayWithMe
//
//  Created by Andy Leece on 3/21/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainGameVC.h"

@interface SettingsVC : UIViewController

@property (nonatomic, weak) MainGameVC *delegate;

@end
