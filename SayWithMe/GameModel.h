//
//  GameModel.h
//  SayWithMe
//
//  Created by Andy Leece on 2/18/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GameModel : NSObject

- (id)init;

- (NSInteger)dataLength;
- (NSString *)randomWordForName:(NSString *)name;
- (NSString *)nameForRow:(NSInteger)row;

- (UIImage *)getRandomImage;
- (NSString *)getNameForImage:(UIImage *)image;
- (NSInteger)getBonusForImageName:(NSString *)imageName;

@end
