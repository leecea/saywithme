//
//  SoundHelper.h
//  ColorTap
//
//  Created by Andy Leece on 1/25/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundHelper : NSObject

+ (SoundHelper *)sharedInstance;
- (void)playCoinLossSound;

@end
