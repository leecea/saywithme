//
//  GameCollectionVC.h
//  SayWithMe
//
//  Created by Andy Leece on 2/16/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainGameVC.h"
#import "GameModel.h"

static const int GRID_WIDTH =  3;
static const int GRID_HEIGHT = 2;

@interface GameCollectionVC : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) MainGameVC *delegate;
@property (nonatomic, weak) GameModel *gameModel;

- (void)stopGrid;
- (void)startGridWithReload:(BOOL)reload;

@end
