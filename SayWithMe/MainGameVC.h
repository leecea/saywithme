//
//  MainGameVC.h
//  SayWithMe
//
//  Created by Andy Leece on 2/16/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainGameVC : UIViewController

- (void)gameCompletedWithName:(NSString *)cellLabel andImage:(NSString *)imageName;
- (void)startGameWithGridReload:(BOOL)reload;
- (void)stopGame;

- (void)hideButton:(BOOL)boolValue;

@property (nonatomic, assign) float speechUtteranceVolume;

@end
