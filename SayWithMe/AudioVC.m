//
//  AudioVC.m
//  SayWithMe
//
//  Created by Andy Leece on 3/14/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "AudioVC.h"

static NSString *const audioFileName = @"audioFile.m4a";
static NSTimeInterval maxRecordTime = 5.0;
static NSTimeInterval progressViewUpdateInterval = 0.1;

@interface AudioVC () 

@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, strong) AVAudioPlayer *player;

@end

@implementation AudioVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self roundTheEdges:self.recordButton];
    [self roundTheEdges:self.playButton];
    [self roundTheEdges:self.stopButton];
    
    [self displayAudioView:NO];
    
    [self initializeRecorder];
}

- (void)runProgressBar
{
    if (self.recorder.recording && self.progressBar.progress < 1){
        
        [self.progressBar setProgress:(self.progressBar.progress + progressViewUpdateInterval/maxRecordTime) animated:YES];
        [NSTimer scheduledTimerWithTimeInterval:progressViewUpdateInterval target:self selector:@selector(runProgressBar) userInfo:nil repeats:NO];
    }
}

// Called by delegate to show or hide audio view

- (void)displayAudioView:(BOOL)boolValue
{
    [self stopButtonHidden:YES];
    
    self.progressBar.hidden = YES;
    
    self.view.hidden = !boolValue;
}

#pragma mark - Audio management

- (void)initializeRecorder
{
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               audioFileName,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSDictionary *recordSetting = @{AVFormatIDKey :[NSNumber numberWithInt:kAudioFormatMPEG4AAC],
                                    AVSampleRateKey : [NSNumber numberWithFloat:44100.0],
                                    AVNumberOfChannelsKey : [NSNumber numberWithInt: 2]
                                   };
    
    // Initiate the recorder
    self.recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    self.recorder.delegate = self;
    self.recorder.meteringEnabled = YES;
    
    [self.recorder prepareToRecord];
}

#pragma mark - Protocol methods

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    [self stopButtonHidden:YES];
    [self.delegate hideButton:NO];
}

#pragma mark - Button management

- (IBAction)recordPressed:(id)sender
{
    [self stopButtonHidden:NO];
    [self.delegate hideButton:YES];
    
    if (self.player.playing) [self.player stop];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    // Start recording
    [self.recorder recordForDuration:maxRecordTime];
    
    // Activate progress bar
    self.progressBar.progress = 0.0;
    [self runProgressBar];
}

- (IBAction)stopPressed:(id)sender
{
    if(self.recorder.recording) [self.recorder stop];
}

- (IBAction)playPressed:(id)sender
{
    if (self.player.playing) return;
    
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.recorder.url error:nil];
    [self.player setDelegate:self];
    [self.player play];
}

- (void)stopButtonHidden:(BOOL)boolValue
{
    self.stopButton.hidden = boolValue;
    self.progressBar.hidden = boolValue;

    self.recordButton.hidden = !boolValue;
    self.playButton.hidden = !boolValue;
}

- (void)roundTheEdges:(UIButton *)button
{
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
