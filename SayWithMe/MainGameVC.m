//
//  MainGameVC.m
//  SayWithMe
//
//  Created by Andy Leece on 2/16/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "MainGameVC.h"
#import "GameCollectionVC.h"
#import "GameModel.h"
#import "SoundHelper.h"
#import "AudioVC.h"
#import "SettingsVC.h"

#import <AVFoundation/AVFoundation.h>

// Constants
//
static long const coinViewTagOffset = 1;
static float const timerInterval = 1.0;
static long const numberOfTimerCoins = 6;
static long const maxNumberOfCoinRows = 3;
static long const coinsLostToPenalty = 6;
static float const animationDuration = 0.3;
static float const animationDelayPerCoin = 0.15;
static long const yOffsetForStartOfAnimation = -300;
static long const pirateImageTag = 500;

// Button labels for different game states
static NSString *const buttonStartGame = @"Start!";
static NSString *const buttonNextTurn = @"Go Again!";
static NSString *const buttonGameDone = @"Done";
static NSString *const buttonStartNewGame = @"Start Again?";
static NSString *const buttonTryAgain = @"Try Again";

// variables that maintain the score
//
long timerCoinsVisible;
long scoreCoins = 0;
long numberOfCoinRows = 0;

@interface MainGameVC ()

@property (nonatomic, strong) AVSpeechSynthesizer *speak;

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *containerAudioView;
@property (weak, nonatomic) IBOutlet UIButton *setupButton;

@property (strong, nonatomic) UIImage *coinImage;
@property (strong, nonatomic) UIImage *pirateImage;

@property (strong, nonatomic) NSTimer *gameTimer;

@property (strong, nonatomic) AudioVC *audioController;

@property (strong, nonatomic) GameCollectionVC *gameGridVC;
@property (strong, nonatomic) GameModel *gameModel;

@property (strong, nonatomic) UIFont *messageFontNormal;
@property (strong, nonatomic) UIFont *messageFontLarge;
@property (assign, nonatomic) CGRect bonusFrame;

@end

@implementation MainGameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.speak = [[AVSpeechSynthesizer alloc] init];
    
    self.coinImage = [UIImage imageNamed:@"Coin.png"];
    self.pirateImage = [UIImage imageNamed:@"Pirate.png"];
    
    self.messageFontNormal = [UIFont fontWithName:@"HelveticaNeue" size:48];
    self.messageFontLarge = [UIFont fontWithName:@"HelveticaNeue" size:96];
    
    [self.backgroundView bringSubviewToFront:self.bottomView];
    [self.backgroundView bringSubviewToFront:self.sideView];
    [self.backgroundView bringSubviewToFront:self.containerView];
    [self.backgroundView bringSubviewToFront:self.setupButton];
    
    
    self.textView.layer.cornerRadius = 10;
    self.textView.layer.borderWidth = 5;
    self.textView.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    self.button.layer.masksToBounds = YES;
    self.button.layer.cornerRadius = 10;

    [self.backgroundView bringSubviewToFront:self.textView];
    [self.backgroundView bringSubviewToFront:self.button];
    [self.backgroundView bringSubviewToFront:self.containerAudioView];
    
    [self displayTextViewWithContent:@"\nAre you ready?" andFont:self.messageFontNormal andButtonLabel:buttonStartGame];
    
    [SoundHelper sharedInstance];       // initialize sounds
    
    timerCoinsVisible = numberOfTimerCoins;
}

- (IBAction)buttonPressed:(id)sender
{
    
    [self showTextAndButton:NO];
    [self.audioController displayAudioView:NO];
    
    UIButton *button = sender;
    
    // The button label tells us about the state of the game and is used to determine some next actions
    
    // Determine if the grid needs to be reloaded
    BOOL reloadGrid = [button.titleLabel.text isEqualToString:buttonNextTurn];
    
    // If we have completed a round and are starting a new game, remove the pirate view that shows when a round is won
    if([button.titleLabel.text isEqualToString:buttonStartNewGame]) [self removePirateFromView:self.view];
    
    // If the round was just completed, show the completed round message
    if([button.titleLabel.text isEqualToString:buttonGameDone]){
        [self showRoundCompleteMessage];
    }
    else [self startGameWithGridReload:reloadGrid];
}

// Called by Audio Controller to hide the button during recording and display it when recording ends

- (void)hideButton:(BOOL)boolValue
{
    self.button.hidden = boolValue;
}

#pragma mark - Utility methods 

- (void)gameCompletedWithName:(NSString *)cellLabel andImage:(NSString *)imageName
{
    // Add coins to the score.  If we can't add all the coins, round must be over.
    //
    BOOL didAddCoins = [self incrementScoreBy:timerCoinsVisible forImage:(NSString *)imageName];
    
    // Get a word to display
    NSString *word = [self.gameModel randomWordForName:cellLabel];
    
    [self displayTextViewWithContent:word andFont:self.messageFontLarge andButtonLabel:(didAddCoins ? buttonNextTurn : buttonGameDone)];
    
    [self.audioController displayAudioView:YES];
}

- (void)showRoundCompleteMessage
{
    [self resetBonusCoinsToZero];
    
    [self addPirateToView:self.view];
    
    [self displayTextViewWithContent:@"\nGreat job!\nYou won this round!" andFont:self.messageFontNormal andButtonLabel:buttonStartNewGame];
}

- (void)displayTextViewWithContent:(NSString *)content andFont:(UIFont *)font andButtonLabel:(NSString *)buttonLabel
{
    self.textView.text = content;
    
    self.textView.textColor = [UIColor whiteColor];
    self.textView.font = font;
    self.textView.textAlignment = NSTextAlignmentCenter;
    
    [self.button setTitle:buttonLabel forState:UIControlStateNormal];
    [self showTextAndButton:YES];
    
    [self speakString:content];
}

- (void)speakString:(NSString *)message
{
    float rateMult = 0.2;
    
    AVSpeechUtterance *speechUtter = [AVSpeechUtterance speechUtteranceWithString:message];
    
    speechUtter.rate = AVSpeechUtteranceMinimumSpeechRate + (AVSpeechUtteranceMaximumSpeechRate-AVSpeechUtteranceMinimumSpeechRate)*rateMult;
    speechUtter.volume = self.speechUtteranceVolume;
    
    [self.speak speakUtterance:speechUtter];
}

#pragma mark - Pirate

- (void)removePirateFromView:(UIView *)view
{
    UIView *pirateView = [view viewWithTag:pirateImageTag];
    CGRect frame = pirateView.frame;
    frame.origin.x = -pirateView.frame.size.width;
    
    [self animateView:pirateView toNewFrame:frame withDelay:0.0 andCompletion:^(BOOL finished) {
        [pirateView removeFromSuperview];
    }];
}

- (void)addPirateToView:(UIView *)view
{
    UIImageView *pirateView = [[UIImageView alloc] initWithImage:self.pirateImage];
    
    CGRect frame = CGRectMake(-pirateView.frame.size.width, (self.view.frame.size.height - pirateView.frame.size.height), pirateView.frame.size.width, pirateView.frame.size.height);
    pirateView.tag = pirateImageTag;
    pirateView.frame = frame;
    
    [view addSubview:pirateView];
    
    frame.origin.x = 0.0;
    
    [self animateView:pirateView toNewFrame:frame withDelay:0.0 andCompletion:nil];
}

#pragma mark - Score control

// Returns BOOL value showing whether we could add the requested number of coins
//
- (BOOL)incrementScoreBy:(NSInteger)timerCoinsVisible forImage:(NSString *)imageName
{
    NSInteger bonusCoins = timerCoinsVisible + [self.gameModel getBonusForImageName:imageName];
    NSInteger loopCounter = bonusCoins;
    
    float animationDelay;
    
    while(loopCounter--){
        
        animationDelay = [self calculatedAnimationDelayFor:loopCounter fromTotal:bonusCoins];
        
        if(![self addBonusCoinWithDelay:animationDelay]) return NO;
    }
    return YES;
}

// Returns BOOL value showing whether it could add the new coin
//
- (BOOL)addBonusCoinWithDelay:(float)delayInSecs
{
    UIImageView *coinView = [[UIImageView alloc] initWithImage:self.coinImage];
    coinView.tag = scoreCoins + coinViewTagOffset;
    
    if(scoreCoins == 0){
        
        // Add first coin at bottom left
        self.bonusFrame = CGRectMake(0.0, (self.bottomView.frame.size.height - coinView.frame.size.height), coinView.frame.size.width, coinView.frame.size.height);
        numberOfCoinRows = 1;
    }
    else {
        
        // Add next coin to the right of previous coin
        UIView *prevCoin = [self.bottomView viewWithTag:(scoreCoins + coinViewTagOffset - 1)];
        
        NSInteger newCoinX = prevCoin.frame.origin.x + (prevCoin.frame.size.width/2);
        NSInteger newCoinY = prevCoin.frame.origin.y;
        
        // Check for the next coin being off the edge of the view.  If so, see if we can add a new line of coins.
        //
        if((newCoinX + coinView.frame.size.width) > self.bottomView.frame.size.width){
            
            if(numberOfCoinRows < maxNumberOfCoinRows) {
            
                // bump up to the next line
                newCoinX = 0;
                newCoinY -= coinView.frame.size.height/2;
                numberOfCoinRows++;
            }
            else return NO;         // Cannot add another line, so can't add a new coin
        }
    
        self.bonusFrame = CGRectMake(newCoinX, newCoinY, coinView.frame.size.width, coinView.frame.size.height);
    }
    
    // Add the coin view
    coinView.frame = [self startFrameUsing:self.bonusFrame];        // set the start frame for the animation
    [self.bottomView addSubview:coinView];
    
    [self animateView:coinView toNewFrame:self.bonusFrame withDelay:delayInSecs andCompletion:nil];         // animate move to final position
    
    scoreCoins++;      // Update the score
    
    return YES;
}

- (void)resetBonusCoinsToZero
{
    [self decrementScoreBy:scoreCoins withAnimation:NO];
    scoreCoins = 0;
    numberOfCoinRows = 0;
}

- (void)decrementScoreBy:(NSInteger)numberToRemove withAnimation:(BOOL)useAnimation
{
    NSInteger total;
    float animationDelay;
    CGRect returnToFrame;
    
    if(numberToRemove > scoreCoins) numberToRemove = scoreCoins;    // can't remove more coins that we have!
    total = numberToRemove;
    
    while(numberToRemove--){
        
        // Find this coin's view and decrement overall coin count
        //
        UIView *view = [self.bottomView viewWithTag:((--scoreCoins) + coinViewTagOffset)];
        
        // Remove the view
        //
        if(useAnimation){
        
            animationDelay = [self calculatedAnimationDelayFor:numberToRemove fromTotal:total];
            returnToFrame = [self startFrameUsing:view.frame];
            
            [self animateView:view toNewFrame:returnToFrame withDelay:animationDelay andCompletion:^(BOOL finished) {
                [view removeFromSuperview];
            }];
        }
        else [view removeFromSuperview];
    }
}

#pragma mark - Animation

- (void)animateView:(UIView *)view toNewFrame:(CGRect)newFrame withDelay:(float)delayInSecs andCompletion:(void (^)(BOOL finished))completionBlock
{
    [UIView animateWithDuration:animationDuration
                          delay:delayInSecs
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.frame = newFrame;
                     }
                     completion:completionBlock];
}

- (CGRect)startFrameUsing:(CGRect)endFrame
{
    CGRect startFrame = endFrame;
    startFrame.origin.x = self.bottomView.frame.size.width;
    startFrame.origin.y += yOffsetForStartOfAnimation;
    
    return startFrame;
}

- (float)calculatedAnimationDelayFor:(NSInteger)thisCoin fromTotal:(NSInteger)totalCoins
{
    return (animationDelayPerCoin * totalCoins)*(1.0 - ((float)thisCoin/(float)totalCoins));
}

#pragma mark - Game control

- (void)startGameWithGridReload:(BOOL)reload
{
    [self.gameGridVC startGridWithReload:reload];
    [self resetTimerCoins];
    self.gameTimer = [NSTimer scheduledTimerWithTimeInterval:timerInterval target:self selector:@selector(decrementTimerCoins) userInfo:nil repeats:YES];
}

- (void)stopGame
{
    [self.gameTimer invalidate];
    [self.gameGridVC stopGrid];
}

- (void)showTextAndButton:(BOOL)show
{
    self.textView.hidden = !show;
    self.button.hidden = !show;
    self.gameGridVC.view.hidden = show;
}

#pragma mark - Manage timer coins

- (void)decrementTimerCoins
{
    if(timerCoinsVisible > 0){
        
        UIView *coinView = [self.sideView viewWithTag:(--timerCoinsVisible)];
        coinView.hidden = YES;
        [[SoundHelper sharedInstance] playCoinLossSound];
        
        if(timerCoinsVisible == 0){
            [self stopGame];
            [self decrementScoreBy:coinsLostToPenalty withAnimation:YES];
            [self displayTextViewWithContent:@"\nToo slow..." andFont:self.messageFontNormal andButtonLabel:buttonTryAgain];
        }
    }
}

- (void)resetTimerCoins
{
    timerCoinsVisible = numberOfTimerCoins;
    for(int tag = 0; tag < timerCoinsVisible; tag++){
        UIView *coinView = [self.sideView viewWithTag:tag];
        coinView.hidden = NO;
    }
}

- (NSInteger)numberOfCoinsVisible
{
    return timerCoinsVisible;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:@"GridSegue"]){
        
        self.gameGridVC = segue.destinationViewController;
        self.gameGridVC.delegate = self;
        
        self.gameModel = [[GameModel alloc] init];
        self.gameGridVC.gameModel = self.gameModel;
    }
    
    if([segue.identifier isEqualToString:@"AudioSegue"]){
        
        self.audioController = segue.destinationViewController;
        self.audioController.delegate = self;
    }
    
    if([segue.identifier isEqualToString:@"SettingsSegue"]){
        
        SettingsVC *settingsVC = segue.destinationViewController;
        settingsVC.delegate = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
