//
//  main.m
//  SayWithMe
//
//  Created by Andy Leece on 2/16/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
