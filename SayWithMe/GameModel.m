//
//  GameModel.m
//  SayWithMe
//
//  Created by Andy Leece on 2/18/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "GameModel.h"

static NSString *const NAME =      @"Name";
static NSString *const WORD =      @"Word";
static NSString *const IMAGE =     @"Image";
static NSString *const BONUS =     @"Bonus";

@interface GameModel()

@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSString *lastWordPicked;

@end

@implementation GameModel

- (id)init
{
    self = [super init];
    
    self.lastWordPicked = @"";
    
    self.imageArray = @[@{NAME : @"Chest", IMAGE : [UIImage imageNamed:@"Chest.png"], BONUS : @2},
                        @{NAME : @"Sack", IMAGE: [UIImage imageNamed:@"Sack.png"], BONUS : @1},
                        @{NAME : @"Purse", IMAGE : [UIImage imageNamed:@"Purse.png"], BONUS : @0}
                      ];

    
    self.dataArray = @[@{NAME : @"Initial P", WORD : @"pan"},
                       @{NAME : @"Initial P", WORD : @"pet"},
                       @{NAME : @"Initial P", WORD : @"paw"},
                       @{NAME : @"Final P", WORD : @"hoop"},
                       @{NAME : @"Final P", WORD : @"top"},
                       @{NAME : @"Final P", WORD : @"hop"},
                       @{NAME : @"Final P", WORD : @"dip"},
                       @{NAME : @"Initial T", WORD : @"team"},
                       @{NAME : @"Initial T", WORD : @"time"},
                       @{NAME : @"Final T", WORD : @"boat"},
                       @{NAME : @"Final T", WORD : @"night"},
                       @{NAME : @"Final T", WORD : @"bite"},
                       @{NAME : @"Final T", WORD : @"white"},
                       @{NAME : @"Initial N", WORD : @"knee"},
                       @{NAME : @"Initial N", WORD : @"nest"},
                       @{NAME : @"Initial N", WORD : @"new"},
                       @{NAME : @"Initial N", WORD : @"nap"},
                       @{NAME : @"Final N", WORD : @"man"},
                       @{NAME : @"Final N", WORD : @"moon"}
                     ];
    return self;
}

#pragma mark - Image Handling

- (UIImage *)getRandomImage
{
    NSDictionary *dict = self.imageArray[arc4random_uniform([self.imageArray count])];
    
    return [dict valueForKey:IMAGE];
}

- (NSString *)getNameForImage:(UIImage *)image
{
    for (NSDictionary *dict in self.imageArray){
        if(image == [dict valueForKey:IMAGE]) return [dict valueForKey:NAME];
    }
    return nil;
}

- (NSInteger)getBonusForImageName:(NSString *)imageName
{
    for (NSDictionary *dict in self.imageArray){
        if([imageName isEqualToString:[dict valueForKey:NAME]]) return [((NSNumber *)[dict valueForKey:BONUS]) integerValue];
    }
    return 0;
}

#pragma mark - Data Handling

- (NSInteger)dataLength
{
    return self.dataArray.count;
}

- (NSInteger)wordsForName:(NSString *)name
{
    int count = 0;
    for (NSDictionary *dict in self.dataArray){
        if([name isEqualToString:[dict valueForKey:NAME]]) count++;
    }
    return count;
}

- (NSString *)randomWordForName:(NSString *)name
{
    NSString *word;
    NSInteger numWords = [self wordsForName:name];
    
    // We will not pick the same word twice in a row, unless the "name" only has one word
    //
    do {
        
        word = [self wordForName:name atIndex:arc4random_uniform(numWords)];
        
    } while (numWords > 1 && [word isEqualToString:self.lastWordPicked]);
    
    self.lastWordPicked = word;
    
    return word;
}

- (NSString *)wordForName:(NSString *)name atIndex:(NSInteger)index
{
    int count = 0;
    for (NSDictionary *dict in self.dataArray){
        
        if([name isEqualToString:[dict valueForKey:NAME]]){
            
            if(count == index) return [dict valueForKey:WORD];   // this is the word we need
            count++;
        }
    }
    return [NSString stringWithFormat:@"Error: word not found for %@:%d", name, index];
}

- (NSString *)nameForRow:(NSInteger)row
{
    NSDictionary *dict = self.dataArray[row];
    return [dict valueForKey:NAME];
}

@end
