//
//  SettingsVC.m
//  SayWithMe
//
//  Created by Andy Leece on 3/21/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "SettingsVC.h"

static NSString *const SYNTH_VOLUME = @"SynthVolume";
static float const synthVolumeDefault = 0.6;

@interface SettingsVC ()

@property (weak, nonatomic) IBOutlet UILabel *synthVolume;
@property (weak, nonatomic) IBOutlet UIStepper *synthVolumeStepper;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initDefaultSettings];
}

- (void)initDefaultSettings
{
    // Load default settings
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:SYNTH_VOLUME]){
        self.synthVolumeStepper.value = [defaults doubleForKey:SYNTH_VOLUME];
    }
    else self.synthVolumeStepper.value = synthVolumeDefault;
    
    [self displaySynthVolume:self.synthVolumeStepper.value];
}

- (void)displaySynthVolume:(float)volume
{
    self.synthVolume.text = [NSString stringWithFormat:@"%d",(int)(volume * 10)];
}

- (IBAction)stepperPressed:(id)sender {
    
    [self displaySynthVolume:self.synthVolumeStepper.value];
    
    // Save new default
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:self.synthVolumeStepper.value forKey:SYNTH_VOLUME];
    [defaults synchronize];
    
    // Update the volume for the game
    self.delegate.speechUtteranceVolume = self.synthVolumeStepper.value;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
