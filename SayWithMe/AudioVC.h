//
//  AudioVC.h
//  SayWithMe
//
//  Created by Andy Leece on 3/14/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MainGameVC.h"

@interface AudioVC : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>

@property (nonatomic, weak) MainGameVC *delegate;

- (void)displayAudioView:(BOOL)boolValue;

@end
