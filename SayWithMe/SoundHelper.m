//
//  SoundHelper.m
//  ColorTap
//
//  Created by Andy Leece on 1/25/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "SoundHelper.h"
#import <AudioToolbox/AudioToolbox.h>

NSURL *soundFileURL;
NSString *soundFilePath;

static NSString *const TIMER_COIN_LOSS =      @"coinLoss";
static NSString *const SOUND_TYPE =           @"wav";

SystemSoundID coinLoss;

static SoundHelper *sharedSoundHelper = nil;

@implementation SoundHelper

+ (SoundHelper *)sharedInstance
{
    if(sharedSoundHelper == nil) sharedSoundHelper = [[self alloc] init];
    return sharedSoundHelper;
}

- (id)init
{
    self = [super init];
    
    // init sounds
    [self initSoundID:&coinLoss fromFile:TIMER_COIN_LOSS];
    
    return self;
}

- (void)initSoundID:(SystemSoundID *)soundId fromFile:(NSString *)soundFileName
{
    soundFilePath = [[NSBundle mainBundle] pathForResource:soundFileName ofType:SOUND_TYPE];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundFileURL, soundId);
}

- (void)playCoinLossSound
{
    AudioServicesPlaySystemSound(coinLoss);
}

@end
