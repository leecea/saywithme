//
//  GameCollectionVC.m
//  SayWithMe
//
//  Created by Andy Leece on 2/16/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "GameCollectionVC.h"

static CGFloat const animationRotation = (M_PI * (3.0) / 180.0);    //the amount of rotation for bonus 'jiggle'

static NSInteger const cellImageTag = 100;
static NSInteger const cellLabelTag = 200;

@interface GameCollectionVC ()

@property (nonatomic, assign) CGSize cellSize;
@property (nonatomic, assign) CGRect imageFrameSize;
@property (nonatomic, assign) CGRect labelFrameSize;
@property (nonatomic, strong) UIFont *labelFont;
@property (nonatomic, assign) BOOL animateDuringCellConfig;

@end

@implementation GameCollectionVC

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init some values
    //
    self.labelFont = [UIFont fontWithName:@"HelveticaNeue" size:24];
    self.cellSize = CGSizeZero;
    self.animateDuringCellConfig = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"GridCell"];
}

- (void)viewWillLayoutSubviews
{
    // get cell size (only get it once)
    //
    if(CGSizeEqualToSize(self.cellSize, CGSizeZero)){
        
        self.cellSize = CGSizeMake(self.collectionView.frame.size.width/GRID_WIDTH, self.collectionView.frame.size.height/GRID_HEIGHT);
        
        // Build the picture views that will be inserted into each cell
        //
        float widthValue = self.cellSize.width/3.0;
        float heightValue = self.cellSize.height/3.0;
        
        self.imageFrameSize = CGRectMake(widthValue, heightValue, widthValue, heightValue);
        
        // Create the cell label frame to position it under the image and take the full width of the cell
        //
        self.labelFrameSize = CGRectMake(0.0, (2*heightValue), self.cellSize.width, heightValue/2.0);
    }
}

#pragma mark - Game Control

- (void)stopGrid
{
    [self animateVisibleCells:NO forCollectionView:self.collectionView];
}

- (void)startGridWithReload:(BOOL)reload
{
    // For a grid reload, do the animation when the cells are configured
    // Otherwise, start the animation directly
    //
    // (Note: If we call animateVisibleCells after calling for a reload, it doesn't work - I assume reload is async)
    //
    if(reload){
        self.animateDuringCellConfig = YES;
        [self.collectionView reloadData];
    }
    else {
        self.animateDuringCellConfig = NO;
        [self animateVisibleCells:YES forCollectionView:self.collectionView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GridCell" forIndexPath:indexPath];
    
    // Configure the cell image view
    //
    UIImageView *imageSubview = (UIImageView *)[cell.contentView viewWithTag:cellImageTag];
    if(!imageSubview){
        imageSubview = [[UIImageView alloc] initWithFrame:self.imageFrameSize];
        imageSubview.tag = cellImageTag;
        [cell.contentView addSubview:imageSubview];
    }

    imageSubview.image = [self.gameModel getRandomImage];
    
    // Animate the image if the flag is set
    //
    if(self.animateDuringCellConfig) [self animateImageView:imageSubview];
    
    // Configure the cell label
    //
    NSInteger dataLen = [self.gameModel dataLength];
    NSString *dataName = [self.gameModel nameForRow:arc4random_uniform(dataLen)];
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:cellLabelTag];
    if(!label){
        label = [[UILabel alloc]initWithFrame:self.labelFrameSize];
        label.tag = cellLabelTag;
        [cell.contentView addSubview:label];
    }
    label.text = dataName;
    label.textColor = [UIColor whiteColor];
    label.font = self.labelFont;
    label.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

#pragma mark - Animate image views

- (void)animateVisibleCells:(BOOL)animate forCollectionView:(UICollectionView *)collectionView
{
    for(UICollectionViewCell *cell in [collectionView visibleCells]){
        
        if(animate) [self animateImageView:cell.contentView.subviews[0]];
        else        [self stopAnimatingImageView:cell.contentView.subviews[0]];
    }
}

- (void)animateImageView:(UIView *)viewToAnimate
{
    // add a random element to how much each view wiggles
    //
    CGFloat randMultiplier = (50.0 + arc4random_uniform(50))/100.0;
    CGAffineTransform leftWobble = CGAffineTransformMakeRotation(-1.0 * randMultiplier * animationRotation);
    CGAffineTransform rightWobble = CGAffineTransformMakeRotation(randMultiplier * animationRotation);
    
    viewToAnimate.transform = leftWobble;  // starting point
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         viewToAnimate.transform = rightWobble;
                         //viewToAnimate.transform = leftWobble;
                     }
                     completion:nil];
}

- (void)stopAnimatingImageView:(UIView *)viewToAnimate
{
    [viewToAnimate.layer removeAllAnimations];
    viewToAnimate.transform = CGAffineTransformIdentity;
}

#pragma mark - Cell layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate stopGame];
    
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    
    UIImageView *imageSubview = (UIImageView *)[cell.contentView viewWithTag:cellImageTag];
    NSString *nameForImage = [self.gameModel getNameForImage:imageSubview.image];
    
    NSString *cellLabel = ((UILabel *)[cell.contentView viewWithTag:cellLabelTag]).text;
    
    [self.delegate gameCompletedWithName:cellLabel andImage:nameForImage];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
